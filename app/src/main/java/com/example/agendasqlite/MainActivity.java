package com.example.agendasqlite;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import database.AgendaContactos;
import database.AgendaDBHelper;
import database.Contacto;

public class MainActivity extends AppCompatActivity {

    private TextView lblRespuesta;
    private TextView txtnombre;
    private TextView txtdireccion;
    private TextView txttel1;
    private TextView txttel2;
    private TextView txtnotas;
    private CheckBox chkFavorito;
    private Button btnguardar;
    private TextView lblNombre;
    private EditText txtid;
    private Button btnLimpiar;
    private Button btnGuardar;
    private Button btnListar;
    private AgendaContactos db;
    private Contacto savedContact;
    private long     id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        lblRespuesta = (TextView) findViewById(R.id.lblRespuesta);
        btnguardar = (Button) findViewById(R.id.btnGuardar);
        txtnombre= (EditText)  findViewById(R.id.txtnombre);
        txttel1= (EditText) findViewById(R.id.txttel1);
        lblNombre = (TextView) findViewById(R.id.lblNombre);
        txttel2= (EditText)  findViewById(R.id.txttel2);
        txtdireccion= (EditText)  findViewById(R.id.txtDireccion);
        txtnotas= (EditText)  findViewById(R.id.txtNotas);
        chkFavorito=(CheckBox) findViewById(R.id.chkFav);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnListar = (Button) findViewById(R.id.btnListar);

        db = new AgendaContactos(MainActivity.this);




        btnguardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              int fav=0;
              if(chkFavorito.isChecked()){
                  fav=1;
              }else{
                  fav=0;
              }
                if(txtnombre.getText().toString().matches("") || txttel1.getText().toString().matches("") || txtdireccion.getText().toString().matches("")){

                    Toast.makeText(MainActivity.this,"Olvidaste llenar campos requeridos",Toast.LENGTH_LONG).show();
                }else{

                    Contacto c = new Contacto(1, txtnombre.getText().toString(), txttel1.getText().toString(), txttel2.getText().toString(), txtdireccion.getText().toString(), txtnotas.getText().toString(),fav );
                    db.openDataBase();
                    long idx = db.insertarContacto(c);
                    Toast.makeText(MainActivity.this,"Se añadio el contacto " + txtnombre.getText().toString()+ "  ID:" + idx,Toast.LENGTH_LONG).show();
                    limpiar();
                    db.cerrar();
                }


            }
        });



        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i= new Intent(MainActivity.this,listActivity.class);
                Bundle bObj=new Bundle();

                i.putExtras(bObj);

                startActivityForResult(i,0);

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if(Activity.RESULT_OK == resultCode){
     Contacto contacto = (Contacto) data.getSerializableExtra("contacto");
     savedContact=contacto;
     id = contacto.get_ID();
     txtnombre.setText((contacto.getNombre()));
     txttel1.setText(contacto.getTelefono1());
     txttel2.setText(contacto.getTelefono2());
     txtdireccion.setText(contacto.getDomicilio());
     txtnotas.setText((contacto.getNotas()));
     if(contacto.getFavorito() > 0){
         chkFavorito.setChecked(true);
     }

 } else{
     limpiar();
 }
    }



    public void limpiar(){
        txtnombre.setText("");
        txttel1.setText("");
        txttel2.setText("");
        txtdireccion.setText("");
        txtnotas.setText("");
        chkFavorito.setChecked(false);
    }
}
